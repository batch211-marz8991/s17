/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	
	//first function here:
	function printWelcomeMessage() {
		let fullName = prompt("What's your name?");
		let age = prompt("How old are you?");
		let address = prompt("Where do you live?");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.")
		console.log("You live in " + address);

	}
	printWelcomeMessage();

	alert("Thankyou for your input!");

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function topFiveBands(){
		console.log("1. The Beatles");
		console.log("2. Eagles");
		console.log("3. Queen");
		console.log("4. Led Zeppelin");
		console.log("5. Pink Floyd");
	}
	topFiveBands();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function topFiveMovies(){
		console.log("1. DON'T WORRY DARLING");
		console.log("Rotten Tomatoes rating: 39%");
		console.log("2.	THE WOMAN KING");
		console.log("Rotten Tomatoes rating: 94%");
		console.log("3. BARBARIAN");
		console.log("Rotten Tomatoes rating: 92%");
		console.log("4. DO REVENGE");
		console.log("Rotten Tomatoes rating: 85%");
		console.log("5. BLONDE");
		console.log("Rotten Tomatoes rating: 50%");
	}
	topFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


// let printFriends = 
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	
};

printUsers();



// console.log(friend1);
// console.log(friend2);






